//
//  SDK.swift
//  CustomFrameworkApp
//
//  Created by Роман Хоменко on 25.07.2022.
//

import Foundation

public struct SDK {
    public static func doSomeWork() {
        print("Do some work here!")
    }
}
